package com.example.demo;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

public class Simple {
        @RequestMapping("/sort")
        public List listing(@RequestParam MultiValueMap<String, String> listing) {
            List<String> names = listing.get("name");
            List<String> sortedList = names.stream().sorted().collect(Collectors.toList());
            return sortedList;
        }
}
